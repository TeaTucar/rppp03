﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions
{
    public static class StavkeExtension
    {
        public static ICollection<int> dohvatiStavke(this IQueryable<IspunjenaStavka> p, int sifraPonude)
        {
            List<int>? popis = new List<int>();
            var count = p.Count(q => q.sifraPonude == sifraPonude);
            if (count > 0)
            {
                popis = p.Where(q => q.sifraPonude == sifraPonude)
                         .Select(q => q.brStavke)
                         .ToList();
            }
            else
                popis = null;
            return popis;
        }
    }
}
