﻿using System;
using System.Linq;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class LokacijaSort
    {
        public static IQueryable<Lokacija> ApplySort(this IQueryable<Lokacija> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Lokacija, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = l => l.sifraGrad;
                    break;
                case 2:
                    orderSelector = l => l.grad;
                    break;
                case 3:
                    orderSelector = l => l.drzava;
                    break;

            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }

            return query;
        }
    }
}
