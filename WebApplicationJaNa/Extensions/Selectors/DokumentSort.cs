﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class DokumentSort
    {
        public static IQueryable<Dokument> ApplySort(this IQueryable<Dokument> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Dokument, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = d => d.naslov;
                    break;
                case 2:
                    orderSelector = d => d.vrsta;
                    break;
                case 3:
                    orderSelector = d => d.urudzbeniBroj;
                    break;
                case 4:
                    orderSelector = d => d.datum;
                    break;
            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}
