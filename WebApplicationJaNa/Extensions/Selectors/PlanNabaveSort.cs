﻿using WebApplicationJaNa.Models;
using System;
using System.Linq;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class PlanNabaveSort
    {
        public static IQueryable<PlanNabave> ApplySort(this IQueryable<PlanNabave> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<PlanNabave, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = t => t.idPlana;
                    break;
                case 2:
                    orderSelector = t => t.godina;
                    break;
                case 3:
                    orderSelector = t => t.OIBNaruciteljaNavigation.naziv;
                    break;

            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}
