﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class NatjecajSort
    {
        public static IQueryable<Natjecaj> ApplySort(this IQueryable<Natjecaj> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Natjecaj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = nat => nat.idNabavaNavigation.predmet;
                    break;
                case 2:
                    orderSelector = nat => nat.OIBNaruciteljaNavigation.naziv;
                    break;
                case 3:
                    orderSelector = nat => nat.datumObjave;
                    break;
                case 4:
                    orderSelector = nat => nat.dostavaElektronickimPutem;
                    break;
                case 5:
                    orderSelector = nat => nat.rokZaDostavu;
                    break;
            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}
