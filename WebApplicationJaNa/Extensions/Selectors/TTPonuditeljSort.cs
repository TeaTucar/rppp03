﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class TTPonuditeljSort
    {

        public static IQueryable<Ponuditelj> ApplySort(this IQueryable<Ponuditelj> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Ponuditelj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.nazivPonuditelj;
                    break;
                case 2:
                    orderSelector = p => p.OIBPonuditelj;
                    break;
                case 3:
                    orderSelector = p => p.adresa;
                    break;
                case 4:
                    orderSelector = p => p.sifraGradNavigation.grad;
                    break;
                case 5:
                    orderSelector = p => p.sifraGradNavigation.drzava;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }
            return query;
        }
    }
}
