﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class TTPonudaSort
    {
        public static IQueryable<Ponuda> ApplySort(this IQueryable<Ponuda> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Ponuda, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.sifraNatjecaj;
                    break;
                case 2:
                    orderSelector = p => p.sifraPonude;
                    break;
                case 3:
                    orderSelector = p => p.OIBPonuditeljNavigation.nazivPonuditelj;
                    break;
                case 4:
                    orderSelector = p => p.sifraKonzorcijNavigation.nazivKonzoricij;
                    break;
            }
            if (orderSelector != null)
            {
                query = ascending ?
                       query.OrderBy(orderSelector) :
                       query.OrderByDescending(orderSelector);
            }
            return query;
        }
    }
}
