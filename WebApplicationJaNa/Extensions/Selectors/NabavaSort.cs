﻿using WebApplicationJaNa.Models;
using System;
using System.Linq;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class NabavaSort
    {
        public static IQueryable<Nabava> ApplySort(this IQueryable<Nabava> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Nabava, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = t => t.idNabava;
                    break;
                case 2:
                    orderSelector = t => t.predmet;
                    break;
                case 3:
                    orderSelector = t => t.CPV;
                    break;
                case 4:
                    orderSelector = t => t.procVrijednost;
                    break;
                case 5:
                    orderSelector = t => t.vrstaPostupka;
                    break;
                case 6:
                    orderSelector = t => t.podjelaGrupe;
                    break;
                case 7:
                    orderSelector = t => t.planiraniPocetak;
                    break;
                case 8:
                    orderSelector = t => t.vrijediOd;
                    break;
                case 9:
                    orderSelector = t => t.vrijediDo;
                    break;
                case 10:
                    orderSelector = t => t.zakon;
                    break;
                case 11:
                    orderSelector = t => t.idPodrucjaNavigation.nazivPodrucja;
                    break;
                case 12:
                    orderSelector = t => t.idPlanaNavigation.godina;
                    break;

            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}