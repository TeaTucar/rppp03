﻿using WebApplicationJaNa.Models;
using System;
using System.Linq;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class NaruciteljSort
    {
        public static IQueryable<Narucitelj> ApplySort(this IQueryable<Narucitelj> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<Narucitelj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = t => t.OIBNarucitelja;
                    break;
                case 2:
                    orderSelector = t => t.naziv;
                    break;
                case 3:
                    orderSelector = t => t.sifraGrad;
                    break;
                case 4:
                    orderSelector = t => t.adresa;
                    break;

            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}