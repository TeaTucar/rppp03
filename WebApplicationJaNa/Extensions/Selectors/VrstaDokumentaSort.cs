﻿using WebApplicationJaNa.Models;
using System;
using System.Linq;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class VrstaDokumentaSort
    {
        public static IQueryable<VrstaDokumenta> ApplySort(this IQueryable<VrstaDokumenta> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<VrstaDokumenta, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = vd => vd.formatDatoteke;
                    break;
                case 2:
                    orderSelector = vd => vd.vrsta;
                    break;
                case 3:
                    orderSelector = vd => vd.opis;
                    break;
            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}
