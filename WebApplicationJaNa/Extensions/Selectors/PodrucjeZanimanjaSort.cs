﻿using WebApplicationJaNa.Models;
using System;
using System.Linq;

namespace WebApplicationJaNa.Extensions.Selectors
{
    public static class PodrucjeZanimanjaSort
    {
        public static IQueryable<PodrucjeZanimanja> ApplySort(this IQueryable<PodrucjeZanimanja> query, int sort, bool ascending)
        {
            System.Linq.Expressions.Expression<Func<PodrucjeZanimanja, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = t => t.idPodrucja;
                    break;
                case 2:
                    orderSelector = t => t.nazivPodrucja;
                    break;

            }
            if (orderSelector != null)
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);

            return query;
        }
    }
}
