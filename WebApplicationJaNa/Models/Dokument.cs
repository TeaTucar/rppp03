﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace WebApplicationJaNa.Models
{
    public partial class Dokument
    {
        public string naslov { get; set; }
        public DateTime datum { get; set; }
        public string urudzbeniBroj { get; set; }
        public string klasa { get; set; }
        public byte[] datoteka { get; set; }
        public string vrsta { get; set; }
        public string idNabava { get; set; }
        public int? sifraNatjecaj { get; set; }
        public int? sifraPonude { get; set; }

        public virtual Nabava idNabavaNavigation { get; set; }
        public virtual Natjecaj sifraNatjecajNavigation { get; set; }
        public virtual Ponuda sifraPonudeNavigation { get; set; }
        public virtual VrstaDokumenta vrstaNavigation { get; set; }
    }
}