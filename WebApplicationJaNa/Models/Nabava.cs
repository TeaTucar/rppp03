﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace WebApplicationJaNa.Models
{
    public partial class Nabava
    {
        public Nabava()
        {
            Dokument = new HashSet<Dokument>();
            Natjecaj = new HashSet<Natjecaj>();
        }

        public string idNabava { get; set; }
        public string predmet { get; set; }
        public string CPV { get; set; }
        public decimal procVrijednost { get; set; }
        public string vrstaPostupka { get; set; }
        public string podjelaGrupe { get; set; }
        public string planiraniPocetak { get; set; }
        public DateTime vrijediOd { get; set; }
        public DateTime? vrijediDo { get; set; }
        public string zakon { get; set; }
        public string idPlana { get; set; }
        public string idPodrucja { get; set; }

        public virtual PlanNabave idPlanaNavigation { get; set; }
        public virtual PodrucjeZanimanja idPodrucjaNavigation { get; set; }
        public virtual ICollection<Dokument> Dokument { get; set; }
        public virtual ICollection<Natjecaj> Natjecaj { get; set; }
    }
}