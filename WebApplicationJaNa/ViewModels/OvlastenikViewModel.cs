﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class OvlastenikViewModel
    {
        //oibp,oibn,ime,prezime
        public string oibOvlastenika { get; set; }
        public string nazivNarucitelja { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }
        public ICollection<short>? ocjenePonuda { get; set; }
    }
}
