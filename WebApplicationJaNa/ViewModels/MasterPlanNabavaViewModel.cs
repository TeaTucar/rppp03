﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class MasterPlanNabavaViewModel
    {
        public IEnumerable<MasterPlanNabaveViewModel> PlanNabava { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
