﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class DPPonudeViewModel
    {
        public IEnumerable<DPPonudaViewModel> Ponude { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
