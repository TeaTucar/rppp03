﻿using WebApplicationJaNa.Models;
using System.Collections.Generic;

namespace WebApplicationJaNa.ViewModels
{
    public class NatjecajViewModel
    {
        public IEnumerable<Natjecaj> Natjecaji { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
