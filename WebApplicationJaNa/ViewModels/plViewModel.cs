﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class plViewModel
    {
        public string sifraPlana { get;  set; }
        public int godinaPlana { get; set; }
        public string nazivNaruciteljaPlana { get; set; }

        public IEnumerable<nbViewModel> Nabave{ get; set; }
}
}
