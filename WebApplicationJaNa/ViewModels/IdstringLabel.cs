﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class IdstringLabel
    {
        [JsonPropertyName("label")]
        public string Label { get; set; }
        [JsonPropertyName("id")]
        public string Id { get; set; }
        public IdstringLabel() { }
        public IdstringLabel(string id, string label)
        {
            Id = id;
            Label = label;
        }
    }
}
