﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class MasterPlanNabaveViewModel
    {
        public int godinaPlana { get; set; }

        public ICollection<String> pojedineNabave { get; set; }
        public String nazivNarucitelja { get; set; }

        public String idPlana { get; set; }
    }
}
