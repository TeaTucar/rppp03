﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class AVPonudeViewModel
    {
        public IEnumerable<Ponuda> Ponude { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}
