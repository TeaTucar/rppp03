﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class PlanNabaveViewModel
    {
        public IEnumerable<PlanNabave> PlanNabave { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
