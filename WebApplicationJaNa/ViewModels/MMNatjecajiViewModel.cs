﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class MMNatjecajiViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<MMNatjecajViewModel> Natjecaji { get; set; }
    }
}
