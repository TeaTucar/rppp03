﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class MMLokacijeViewModel
    {
        public IEnumerable<MMLokacijaViewModel> Lokacije { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
