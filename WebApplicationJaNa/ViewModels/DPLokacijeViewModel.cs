﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class DPLokacijeViewModel
    {
        public IEnumerable<DPLokacijaViewModel> Lokacije { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
