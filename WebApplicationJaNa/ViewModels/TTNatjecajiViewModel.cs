﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class TTNatjecajiViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<TTNatjecajViewModel> Natjecaji { get; set; }
    }
}
