﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class TTPonudaViewModel
    {
        public int sifraPonude { get; set; }
        public string nazivPonuditelj { get; set; }
        public int OIBPonuditelj { get; set; }
        public string? nazivKonzorcij { get; set; }
        public int sifraNatjecaj { get; set; }
        public ICollection<int>? stavkeTroskovnika { get; set; }
    }
}
