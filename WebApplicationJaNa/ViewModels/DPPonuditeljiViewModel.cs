﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class DPPonuditeljiViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<DPPonuditeljViewModel> Ponuditelji { get; set; }


    }
}
