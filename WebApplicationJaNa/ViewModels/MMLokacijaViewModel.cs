﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{

    public class MMLokacijaViewModel
    {
        public int sifraGrad { get; set; }
        public string grad { get; set; }
        public string drzava { get; set; }

    }

}
