﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class TTPonudeViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<TTPonudaViewModel> Ponude { get; set; }
    }
}
