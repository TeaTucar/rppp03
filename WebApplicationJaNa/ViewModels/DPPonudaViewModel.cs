﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class DPPonudaViewModel
    {
        public int sifraPonude { get; set; }
        public string OIBPonuditelj { get; set; }
        public int? sifraKonzorcij { get; set; }
        public int sifraNatjecaj { get; set; }
    }
}
