﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class DokumentViewModel
    {
        public IEnumerable<Dokument> Dokumenti { get; set; }
        public Natjecaj Natjecaj { get; set; }
        public Nabava Nabava { get; set; }
        public Narucitelj Narucitelj { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
