﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class TTPonuditeljiViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public IEnumerable<TTPonuditeljViewModel> Ponuditelji { get; set; }
    }
}
