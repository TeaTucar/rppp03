﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class StavkeViewModel
    {
        public PagingInfo PagingInfo { get; set; }
        public Ponuda ponuda { get; set; }
        public ICollection<StavkaTroskovnika> stavke { get; set; }
        public string nazivPonuditelja { get; set; }
        public string? nazivKonzorcija { get; set; }
    }
}
