﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class MMNabavaViewModel
    {
        public IEnumerable<Nabava> MMNabava { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}