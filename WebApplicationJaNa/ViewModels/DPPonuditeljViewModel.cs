﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ViewModels
{
    public class DPPonuditeljViewModel
    {
        public string OIBPonuditelj { get; set; }
        public string nazivPonuditelj { get; set; }
        public int sifraGrad { get; set; }
        public string grad { get; set; }
        public string adresa { get; set; }
        public string drzava { get; set; }
    }
}
