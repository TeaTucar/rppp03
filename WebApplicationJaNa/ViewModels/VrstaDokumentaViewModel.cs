﻿using WebApplicationJaNa.Models;
using System.Collections.Generic;

namespace WebApplicationJaNa.ViewModels
{
    public class VrstaDokumentaViewModel
    {
        public IEnumerable<VrstaDokumenta> VrstaDokumenta { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
