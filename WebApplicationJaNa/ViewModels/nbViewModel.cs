﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class nbViewModel
    {
        public string nazivPodrucjaNabave { get;  set; }
        public string idNabave { get;  set; }
        public decimal vrijednostNabave { get;  set; }
        public string cpvNabave { get;  set; }
        public string predmetNabave { get;  set; }
        public string vrstapostupkaNabave { get;  set; }
        public string podjelagrupeNabave { get;  set; }
        public string planiranipocetakNabave { get;  set; }
        public DateTime vrijediodNabave { get;  set; }
        public DateTime? vrijedidoNabave { get;  set; }
        public string zakonNabave { get; set; }
    }
}
