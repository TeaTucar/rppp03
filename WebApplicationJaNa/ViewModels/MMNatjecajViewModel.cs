﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationJaNa.ViewModels
{
    public class MMNatjecajViewModel
    {
        public int sifraNatjecaj { get; set; }
        public bool dostavaElektronickimPutem { get; set; }
        public string idNabava { get; set; }
        public string predmetNabave { get; set; }
        public string OIBNarucitelja { get; set; }
        public string nazivNarucitelja { get; set; }
        public DateTime datumObjave { get; set; }
        public DateTime rokZaDostavu { get; set; }

    }
}