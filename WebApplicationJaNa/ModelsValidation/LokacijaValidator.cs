﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using FluentValidation;


namespace WebApplicationJaNa.ModelsValidation
{
    public class LokacijaValidator : AbstractValidator<Lokacija>
    {
        public LokacijaValidator()
        {

            RuleFor(l => l.sifraGrad).NotEmpty().WithMessage("Šifra grada je obavezna!");


            RuleFor(l => l.grad).NotEmpty().WithMessage("Ime grada je obavezno!")
                                       .MaximumLength(50).WithMessage("Maksimalna duljina imena grada je 50 znakova!");

            RuleFor(l => l.drzava).NotEmpty().WithMessage("Ime države je obavezno!")
                                       .MaximumLength(50).WithMessage("Maksimalna duljina imena države je 50 znakova!");



        }
    }
}
