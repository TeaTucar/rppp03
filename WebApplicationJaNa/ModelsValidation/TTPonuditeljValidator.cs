﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ModelsValidation
{
    public class TTPonuditeljValidator : AbstractValidator<Ponuditelj>
    {
        public TTPonuditeljValidator()
        {
            RuleFor(p => p.nazivPonuditelj)
                .NotEmpty().WithMessage("Obavezan unos naziva ponuditelja")
                .MaximumLength(25).WithMessage("Duljina naziva mora biti do 25 znakova");

            RuleFor(p => p.OIBPonuditelj)
                .NotEmpty().WithMessage("Obavezan unos OIB-a ponuditelja")
                .MaximumLength(11).WithMessage("OIB mora sadržavati 11 znakova")
                .MinimumLength(11).WithMessage("OIB mora sadržavati 11 znakova");

            RuleFor(p => p.adresa)
                .NotEmpty().WithMessage("Obavezan unos adrese")
                .MaximumLength(50).WithMessage("Duljina adrese mora biti do 50 znakova");
        }
    }
}
