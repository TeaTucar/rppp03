﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using FluentValidation;

namespace WebApplicationJaNa.ModelsValidation
{
    public class PlanNabaveValidator : AbstractValidator<PlanNabave>
    {
        public PlanNabaveValidator()
        {

            RuleFor(t => t.idPlana).NotEmpty().WithMessage("Šifra plana je obavezna!")
                                       .MinimumLength(10).WithMessage("Šifra plana je duljine 10 znakova!")
                                       .MaximumLength(10).WithMessage("Šifra područja je duljine 10 znakova!");

            RuleFor(t => t.godina).NotEmpty().WithMessage("Odabir godine je obavezan!");

            RuleFor(t => t.OIBNarucitelja).NotEmpty().WithMessage("Odabir naručitelja je obavezan!");

        }
    }

}
