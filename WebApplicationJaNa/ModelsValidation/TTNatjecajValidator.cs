﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ModelsValidation
{
    public class TTNatjecajValidator : AbstractValidator<Natjecaj>
    {
        public TTNatjecajValidator()
        {
            RuleFor(n => n.datumObjave)
                .NotEmpty().WithMessage("Obavezan unos datuma objave");

            RuleFor(n => n.rokZaDostavu)
                .NotEmpty().WithMessage("Obavezan unos roka za dostavu");
        }
    }
}
