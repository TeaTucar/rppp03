﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using FluentValidation;

namespace WebApplicationJaNa.ModelsValidation
{
    public class PodrucjeZanimanjaValidator : AbstractValidator<PodrucjeZanimanja>
    {
        public PodrucjeZanimanjaValidator()
        {

         RuleFor(t => t.idPodrucja).NotEmpty().WithMessage("Šifra područja je obavezna!") 
                                    .MinimumLength(10).WithMessage("Šifra područja je duljine 10 znakova!")
                                    .MaximumLength(10).WithMessage("Šifra područja je duljine 10 znakova!");

        RuleFor(t => t.nazivPodrucja).NotEmpty().WithMessage("Naziv područja je obavezan!")
                                     .MaximumLength(50).WithMessage("Maksimalna duljina je 50 znakova!");



        }
    }

}
