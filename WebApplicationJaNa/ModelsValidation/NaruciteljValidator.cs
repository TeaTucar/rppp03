﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using FluentValidation;

namespace WebApplicationJaNa.ModelsValidation
{
    public class NaruciteljValidator : AbstractValidator<Narucitelj>
    {
        public NaruciteljValidator()
        {

            RuleFor(t => t.OIBNarucitelja).NotEmpty().WithMessage("Unos OIB-a je obavezan!")
                                       .MinimumLength(11).WithMessage("OIB mora sadržavati točno 11 znakova!")
                                       .MaximumLength(11).WithMessage("OIB mora sadržavati točno 11 znakova!");

            RuleFor(t => t.naziv).NotEmpty().WithMessage("Unos naziva je obavezan!")
                                    .MaximumLength(100).WithMessage("Naziv ne smije biti veći od 100 znakova!");

            RuleFor(t => t.sifraGrad).NotEmpty().WithMessage("Odabir grada je obavezan!");

            RuleFor(t => t.adresa).MaximumLength(50).WithMessage("Adresa smije sadržavati najviše 50 znakova!");

        }
    }

}
