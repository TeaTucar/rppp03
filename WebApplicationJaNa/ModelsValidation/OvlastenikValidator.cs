﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ModelsValidation
{
    public class OvlastenikValidator : AbstractValidator<Ovlastenik>
    {
        public OvlastenikValidator()
        {

            RuleFor(o => o.OIBOvlastenika).NotEmpty().WithMessage("Unos OIB-a je obavezan!")
                                            .MinimumLength(11).WithMessage("OIB mora sadržavati točno 11 znakova!")
                                            .MaximumLength(11).WithMessage("OIB mora sadržavati točno 11 znakova!");

            RuleFor(o => o.ime).NotEmpty().WithMessage("Unos imena je obavezan!")
                                .MaximumLength(25).WithMessage("Ime smije sadržavati najviše 25 znakova");

            RuleFor(o => o.prezime).NotEmpty().WithMessage("Unos prezimena je obavezan!")
                                .MaximumLength(25).WithMessage("Prezime smije sadržavati najviše 25 znakova");

        }
    }
}
