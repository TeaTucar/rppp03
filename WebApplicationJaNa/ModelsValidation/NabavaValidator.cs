﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using FluentValidation;

namespace WebApplicationJaNa.ModelsValidation
{
    public class NabavaValidator : AbstractValidator<Nabava>
    {
        public NabavaValidator()
        {

            RuleFor(t => t.idNabava).NotEmpty().WithMessage("Šifra nabave je obavezna!")
                                       .MinimumLength(10).WithMessage("Šifra nabave je duljine 10 znakova!")
                                       .MaximumLength(10).WithMessage("Šifra nabave je duljine 10 znakova!");

            RuleFor(t => t.predmet).NotEmpty().WithMessage("Predmet nabave je obavezan!")
                                       .MaximumLength(50).WithMessage("Maksimalna duljina predmeta nabave je 50 znakova!");

            RuleFor(t => t.CPV).NotEmpty().WithMessage("CPV je obavezan!")
                                       .MaximumLength(10).WithMessage("Maksimalna duljina CPV-a je 10 znakova!");

            RuleFor(t => t.procVrijednost).NotEmpty().WithMessage("Procijenjena vrijednost je obavezna!");

            RuleFor(t => t.vrstaPostupka).NotEmpty().WithMessage("Vrsta postupka je obavezna!")
                                       .MaximumLength(50).WithMessage("Maksimalna duljina vrste postupka je 50 znakova!");

            RuleFor(t => t.podjelaGrupe).MaximumLength(10).WithMessage("Maksimalna duljina podjela grupe je 10 znakova!");

            RuleFor(t => t.planiraniPocetak).MaximumLength(10).WithMessage("Maksimalna duljina planiranog početka je 10 znakova!");


            RuleFor(t => t.vrijediOd).NotEmpty().WithMessage("Informacija od kada vrijedi nabava je obavezna!");

             // Validator za datum   
            //RuleFor(t => t.vrijediDo).
              // .MaximumLength(50).WithMessage("Maksimalna duljina podjela grupe je 10 znakova!");

            RuleFor(t => t.zakon).NotEmpty().WithMessage("Unos zakona je obavezan!")
                            .MaximumLength(50).WithMessage("Maksimalna duljina zakona je 50 znakova!");


            RuleFor(t => t.idPlana).NotEmpty().WithMessage("Odabir godine plana nabave je obavezan!");

            RuleFor(t => t.idPodrucja).NotEmpty().WithMessage("Odabir područja zanimanja  je obavezan!");

        }
    }

}
