﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;

namespace WebApplicationJaNa.ModelsValidation
{
    public class OcjenaPonudeValidator : AbstractValidator<OcjenaPonude>
    {
        public OcjenaPonudeValidator()
        {

            RuleFor(t => t.OIBOvlastenika).NotEmpty().WithMessage("Unos OIB-a je obavezan!");

            RuleFor(t => (int)t.ocjena).NotEmpty().WithMessage("Unos ocjene je obavezan!")
                                .GreaterThanOrEqualTo(0).WithMessage("Postotak ne može biti negativan!")
                                .LessThanOrEqualTo(100).WithMessage("Postotak ne može prelaziti 100%");

            RuleFor(t => t.opis).MaximumLength(250).WithMessage("Opis smije sadržavati maksimalno 250 znakova!");

        }
    }
}
