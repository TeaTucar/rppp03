﻿using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplicationJaNa.Extensions.Selectors;
using System.Text.Json;
using WebApplicationJaNa.Extensions;

namespace WebApplicationJaNa.Controllers
{
    public class VrstaDokumentaController : Controller
    {
        private readonly Models.RPPP03Context context;
        private readonly ILogger<VrstaDokumentaController> logger;
        private readonly AppSettings appSettings;

        public VrstaDokumentaController(Models.RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<VrstaDokumentaController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;

            var query = context.VrstaDokumenta
                       .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                logger.LogInformation("Niti jedna vrsta dokumenta nije evidentirana");
                TempData[Constants.Message] = "Niti jedna vrsta dokumenta nije evidentirana";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new ViewModels.PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new
                {
                    page = pagingInfo.TotalPages,
                    sort,
                    ascending
                });
            }
            query = query.ApplySort(sort, ascending);

            var vrsteDok = query
                        .Skip((page - 1) * pagesize)
                        .Take(pagesize)
                        .ToList();

            var model = new ViewModels.VrstaDokumentaViewModel
            {
                VrstaDokumenta = vrsteDok,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]
        public IActionResult Create() { return View(); }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Models.VrstaDokumenta vrstaDokumenta)
        {
            logger.LogTrace(JsonSerializer.Serialize(vrstaDokumenta));
            if (ModelState.IsValid)
            {
                try
                {
                    context.Add(vrstaDokumenta);
                    context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Vrsta Dokumenta {vrstaDokumenta.vrsta} dodana.");
                    TempData[Constants.Message] = $"Vrsta Dokumenta {vrstaDokumenta.vrsta} dodana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanja nove vrste dokumenta: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(vrstaDokumenta);
                }
            }
            else
            {
                return View(vrstaDokumenta);
            }
        }

        [HttpGet]
        public IActionResult Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var vrstaDokumenta = context.VrstaDokumenta.AsNoTracking().Where(vd => vd.vrsta == id).SingleOrDefault();
            if (vrstaDokumenta == null)
            {
                logger.LogWarning("Ne postoji vrsta dokumena: ", id);
                return NotFound("Ne postoji vrsta dokumenta: " + id);
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(vrstaDokumenta);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Models.VrstaDokumenta vrstaDokumenta = await context.VrstaDokumenta
                                  .Where(vd => vd.vrsta == id)
                                  .FirstOrDefaultAsync();
                if (vrstaDokumenta == null)
                {
                    return NotFound("Neispravna vrsta dokumenta: " + id);
                }

                if (await TryUpdateModelAsync<Models.VrstaDokumenta>(vrstaDokumenta, "",
                    vd => vd.vrsta, vd => vd.formatDatoteke, vd => vd.opis))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;
                    try
                    {
                        await context.SaveChangesAsync();
                        TempData[Constants.Message] = "Vrsta dokumenta ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(vrstaDokumenta);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o vrsti dokumenta nije moguće povezati s forme za ažuriranje {0}");
                    return View(vrstaDokumenta);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), id);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string vrsta, int page = 1, int sort = 1, bool ascending = true)
        {
            var vd = context.VrstaDokumenta.Find(vrsta);
            if (vd != null)
            {
                try
                {
                    var v = vd.vrsta; 
                    context.Remove(vd);
                    context.SaveChanges();
                    logger.LogInformation($"Vrsta {v} obrisana");
                    TempData[Constants.Message] = $"Vrsta {v} obrisana";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja vrste: {0}" + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                    logger.LogError("Pogreška prilikom brisanja vrste: {0}" + exc.CompleteExceptionMessage());
                }
            }
            else
            {
                logger.LogWarning("Ne postoji vrsta s oznakom: ", vrsta);
                TempData[Constants.Message] = "Ne postoji vrsta s oznakom: " + vrsta;
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
        }
    }
}
