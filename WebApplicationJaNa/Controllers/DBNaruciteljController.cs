﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class DBNaruciteljController : Controller
    {
        private readonly RPPP03Context context;
        private readonly AppSettings appSettings;
        private readonly ILogger<DBNaruciteljController> logger;


        public DBNaruciteljController(RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<DBNaruciteljController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }
        private async Task PrepareDropdownLists()
        {
            var lokacije = await context.Lokacija.OrderBy(t => t.grad).Select(t => new { t.grad, t.sifraGrad }).ToListAsync();
            ViewBag.Narucitelj = new SelectList(lokacije, nameof(Lokacija.sifraGrad), nameof(Lokacija.grad));
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = context.Narucitelj.Include(t => t.sifraGradNavigation).AsNoTracking();
            int count = query.Count();

            if (count == 0)
            {
                logger.LogInformation("Ne postoji naručitelj.");
                TempData[Constants.Message] = "Ne postoji naručitelj";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages || page < 1)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var narucitelji = query.Skip((page - 1) * pagesize)
                                 .Take(pagesize)
                                 .ToList();

            var model = new DBNaruciteljViewModel
            {
                Narucitelj = narucitelji,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var narucitelji = context.Narucitelj
                .AsNoTracking()
                .Where(t => t.OIBNarucitelja == id)
                .SingleOrDefault();

            if (narucitelji == null)
            {
                return NotFound("Ne postoji naručitelj sa šifrom:" + id);
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(narucitelji);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Narucitelj narucitelji = await context.Narucitelj.FindAsync(id);

                if (narucitelji == null)
                {
                    return NotFound("Neispravan oib naručitelja: " + id);
                }


                if (await TryUpdateModelAsync<Narucitelj>(narucitelji, "", t => t.naziv))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;

                    try
                    {
                        await context.SaveChangesAsync();
                        TempData[Constants.Message] = "Podaci o naručitelju su ažurirani.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        await PrepareDropdownLists();
                        return View(narucitelji);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o naručitelju nije moguće povezati s forme");
                    return View(narucitelji);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { page = page, sort = sort, ascending = ascending });
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string OIBNarucitelja, int page = 1, int sort = 1, bool ascending = true)
        {
            var narucitelji = context.Narucitelj.Find(OIBNarucitelja);
            if (narucitelji != null)
            {
                try
                {
                    string naziv = narucitelji.naziv;
                    context.Remove(narucitelji);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Naručitelj {naziv} uspješno obrisano.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception ex)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja naručitelja: " + ex.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Naručitelj s oibom: " + OIBNarucitelja;
                TempData[Constants.ErrorOccurred] = true;
                return NotFound();

            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });

        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownLists();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Narucitelj narucitelji)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Add(narucitelji);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Naručitelj {narucitelji.OIBNarucitelja} dodao.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(narucitelji);
                }
            }
            else
            {
                return View(narucitelji);
            }
        }


    }
}
