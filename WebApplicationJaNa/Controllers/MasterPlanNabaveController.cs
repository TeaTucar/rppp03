﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class MasterPlanNabaveController : Controller
    {
        private readonly RPPP03Context context;
        private readonly AppSettings appSettings;
        private readonly ILogger<MasterPlanNabaveController> logger;


        public MasterPlanNabaveController(RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<MasterPlanNabaveController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }



        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = context.PlanNabave.Include(t => t.OIBNaruciteljaNavigation).AsNoTracking();
            int count = query.Count();

            if (count == 0)
            {
                logger.LogInformation("Ne postoje planovi nabave.");
                TempData[Constants.Message] = "Ne postoje planovi nabave.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Index));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages || page < 1)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var planoviNabava = query
                                    .Select(p => new MasterPlanNabaveViewModel
                                    {
                                        idPlana = p.idPlana,
                                        godinaPlana = p.godina,
                                        nazivNarucitelja = p.OIBNaruciteljaNavigation.naziv,
                                    }
                                    )
                                    .Skip((page - 1) * pagesize)
                                 .Take(pagesize)
                                 .ToList();

            var queryStavka = context.Nabava.AsNoTracking();
            foreach(MasterPlanNabaveViewModel plan in planoviNabava)
            {
                plan.pojedineNabave = queryStavka.Where(p => p.idPlana.Equals(plan.idPlana)).Select(p => p.predmet).ToList();
            }
            var model = new MasterPlanNabavaViewModel
            {
                PlanNabava = planoviNabava,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        public IActionResult Detail(String id, string filter, int page = 1, int sort = 1, bool ascending = true, string viewName = nameof(Detail))
        {
            ViewBag.Page = page;
            ViewBag.Filter = filter;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            var planNabave = context.PlanNabave
                                    .Where(d => d.idPlana == id)
                                    .Select(d => new plViewModel
                                    {
                                        sifraPlana = d.idPlana,
                                        godinaPlana = d.godina,
                                        nazivNaruciteljaPlana = d.OIBNaruciteljaNavigation.naziv,
                                    })
                                    .SingleOrDefault();
            if (planNabave == null)
            {
                return NotFound("Plan nabave {id} ne postoji");
            }
            else
            {
                // nazpartner ?

                
                //učitavanje stavki - nabava
                var nabave = context.Nabava
                                      .Where(s => s.idPlana == planNabave.sifraPlana)
                                      //.OrderBy(s => s.idNabava)
                                      .Select(s => new nbViewModel
                                      {     
                                          idNabave = s.idNabava,
                                          predmetNabave = s.predmet,
                                          cpvNabave = s.CPV,
                                          vrijednostNabave = s.procVrijednost,
                                          vrstapostupkaNabave = s.vrstaPostupka,
                                          podjelagrupeNabave = s.podjelaGrupe,
                                          planiranipocetakNabave = s.planiraniPocetak,
                                          vrijediodNabave = s.vrijediOd,
                                          vrijedidoNabave = s.vrijediDo,
                                          zakonNabave = s.zakon,
                                          nazivPodrucjaNabave = s.idPodrucjaNavigation.nazivPodrucja,
                                      })
                                      .ToList();

                planNabave.Nabave = nabave;


                return View(planNabave);
            }
        }

       


    }
}