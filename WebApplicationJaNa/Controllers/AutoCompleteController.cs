﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;

namespace WebApplicationJaNa.Controllers
{
    public class AutoCompleteController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appData;

        public AutoCompleteController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        public async Task<IEnumerable<IdstringLabel>> Ponuditelj(string term)
        {
            var query = ctx.Ponuditelj
                           .Select(p => new IdstringLabel
                           {
                               Id = p.OIBPonuditelj,
                               Label = p.nazivPonuditelj
                           })
                           .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                                  .ThenBy(l => l.Id)
                                  .Take(appData.AutoCompleteCount)
                                  .ToListAsync();
            return list;
        }

        public async Task<IEnumerable<IdLabel>> Konzorcij(string term)
        {
            var query = ctx.Konzorcij
                           .Select(k => new IdLabel
                           {
                               Id = k.sifraKonzorcij,
                               Label = k.nazivKonzoricij
                           })
                           .Where(l => l.Label.Contains(term));

            var list = await query.OrderBy(l => l.Label)
                                  .ThenBy(l => l.Id)
                                  .Take(appData.AutoCompleteCount)
                                  .ToListAsync();
            return list;
        }

    }
}
