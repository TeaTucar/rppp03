﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using WebApplicationJaNa.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class OvlastenikController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public OvlastenikController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pageSize = appSettings.PageSize;
            var query = ctx.Ovlastenik.Include(o => o.OIBNaruciteljaNavigation).AsNoTracking();

            int count = query.Count();
            //if count je nula preusmjerit na drugu str
            if (count == 0)
            {
                TempData[Constants.Message] = "Ne postoji zapis o niti jednom ovlašteniku.";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(HomeController.View));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pageSize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Ovlastenik, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = ovl => ovl.OIBOvlastenika;
                    break;
                case 2:
                    orderSelector = ovl => ovl.ime;
                    break;
                case 3:
                    orderSelector = ovl => ovl.prezime;
                    break;
                case 4:
                    orderSelector = ovl => ovl.OIBNaruciteljaNavigation.naziv;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var ovlastenici = query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var ocjene = await query
                          .Select(o => new OvlastenikViewModel
                          {
                              oibOvlastenika = o.OIBOvlastenika,
                              nazivNarucitelja = o.OIBNaruciteljaNavigation.naziv,
                              ime = o.ime,
                              prezime = o.prezime
                          })
                          .Skip((page - 1) * pageSize)
                          .Take(pageSize)
                          .ToListAsync();

            foreach (var ocjena in ocjene)
            {
                ocjena.ocjenePonuda = ctx.OcjenaPonude.Where(op => op.OIBOvlastenika.Equals(ocjena.oibOvlastenika)).Select(op => op.ocjena).ToList();
            }

            var model = new OvlasteniciViewModel
            {
                Ovlastenici = ocjene,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        public async Task<IActionResult> More(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            int pageSize = appSettings.PageSize;
            var queryOvlastenik = ctx.Ovlastenik.Include(o => o.OIBNaruciteljaNavigation).AsNoTracking().Where(o => o.OIBOvlastenika == id);
            var countOvlastenik = queryOvlastenik.Count();

            if (countOvlastenik < 1 || countOvlastenik > 1)
            {
                TempData[Constants.Message] = "Traženi podatak ne postoji.";
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(HomeController.View));
            }

            var query = ctx.OcjenaPonude.Include(o => o.OIBOvlastenikaNavigation.OIBNaruciteljaNavigation).AsNoTracking().Where(o => o.OIBOvlastenika == id);

            var count = query.Count();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pageSize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            //if(count == 0)
            //{
            //    OcjenaPonude op = new OcjenaPonude()
            //}

            System.Linq.Expressions.Expression<Func<OcjenaPonude, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = c => c.sifraPonude;
                    break;
                case 2:
                    orderSelector = c => c.ocjena;
                    break;
                case 3:
                    orderSelector = c => c.datum;
                    break;
                case 4:
                    orderSelector = c => c.opis;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var ocjene = query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var model = new OcjenePonudaViewModel
            {
                OcjenePonuda = ocjene,
                PagingInfo = pagingInfo
            };

            return View(model);
        }
    }
}
