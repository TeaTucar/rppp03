﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;

namespace WebApplicationJaNa.Controllers
{


    public class MMLokacijaController : Controller
    {

        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;


        public MMLokacijaController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appSettings = options.Value;

        }


        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Lokacija.AsNoTracking();
            int count = await query.CountAsync();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };


            query = query.ApplySort(sort, ascending);

            var lokacije = await query
                                .Select(l => new MMLokacijaViewModel
                                {
                                    sifraGrad = l.sifraGrad,
                                    grad = l.grad,
                                    drzava = l.drzava
                                })
                                .Skip((page - 1) * pagesize)
                                .Take(pagesize)
                                .ToListAsync();

            var model = new MMLokacijeViewModel
            {
                Lokacije = lokacije,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


        [HttpGet]
        public IActionResult Create() { return View(); }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Lokacija lokacija)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(lokacija);
                    await ctx.SaveChangesAsync();

                    TempData[Constants.Message] = $"Lokacija {lokacija.grad} dodano. Sifra grada = {lokacija.sifraGrad}";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(lokacija);
                }
            }
            else
            {

                return View(lokacija);
            }
        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int sifraGrad, int page = 1, int sort = 1, bool ascending = true)
        {
            var lokacija = await ctx.Lokacija.FindAsync(sifraGrad);
            if (lokacija != null)
            {
                try
                {
                    string naziv = lokacija.grad;
                    ctx.Remove(lokacija);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Mjesto {naziv} sa šifrom {sifraGrad} obrisano.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja mjesta: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji mjesto sa šifrom: {sifraGrad}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }



        [HttpGet]
        public async Task<IActionResult> Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {

            var lokacija = await ctx.Lokacija
                                  .AsNoTracking()
                                  .Where(l => l.sifraGrad == id)
                                  .SingleOrDefaultAsync();
            if (lokacija != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(lokacija);
            }
            else
            {
                return NotFound($"Neispravna sifra grada: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Lokacija lokacija, int page = 1, int sort = 1, bool ascending = true)
        {
            if (lokacija == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = await ctx.Lokacija.AnyAsync(l => l.sifraGrad == lokacija.sifraGrad);
            if (!checkId)
            {
                return NotFound($"Neispravna šifra lokacije: {lokacija?.sifraGrad}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(lokacija);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = "Lokacija ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index), new { page, sort, ascending });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(lokacija);
                }
            }
            else
            {

                return View(lokacija);
            }
        }

    }


}
