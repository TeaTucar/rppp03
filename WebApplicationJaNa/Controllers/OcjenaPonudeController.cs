﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using WebApplicationJaNa.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class OcjenaPonudeController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public OcjenaPonudeController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        private async Task PrepareDropdownList()
        {
            var ponude = await ctx.Ponuda.OrderBy(t => t.sifraPonude).Select(t => t.sifraPonude).ToListAsync();
            ViewBag.OcjenaPonude = new SelectList(ponude, nameof(Ponuda.sifraPonude));
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownList();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OcjenaPonude ocjenaPonude)
        {
            try
            {
                ocjenaPonude.datum = DateTime.Now;
                ctx.Add(ocjenaPonude);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Ocjena za ponudu {ocjenaPonude.sifraPonude} uspješno dodana.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                return View(ocjenaPonude);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int sifraOcjene, int page = 1, int sort = 1, bool ascending = true)
        {

            var ocjenaPonude = ctx.OcjenaPonude
                .Find(sifraOcjene);
            if (ocjenaPonude == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    int sifra = ocjenaPonude.sifraOcjene;
                    ctx.Remove(ocjenaPonude);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ocjena za ponudu {ocjenaPonude.sifraPonude} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ocjene ponude.";
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            await PrepareDropdownList();
            var ocjenaPonude = ctx.OcjenaPonude.
                AsNoTracking()
                .Where(n => n.sifraOcjene == id)
                .FirstOrDefault();
            if (ocjenaPonude == null)
            {
                return NotFound($"Ne postoji ocjena sa šifrom {id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(ocjenaPonude);
            }
        }
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                OcjenaPonude ocjenaPonude = await ctx.OcjenaPonude.FindAsync(id);
                if (ocjenaPonude == null)
                {
                    return NotFound($"Ne postoji ponuda sa šifrom {id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;

                bool ok = await TryUpdateModelAsync<OcjenaPonude>(ocjenaPonude, "", n => n.sifraPonude, n => n.ocjena, n => n.opis);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Ocjena za ponudu {ocjenaPonude.sifraPonude} uspješno ažurirana.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(ocjenaPonude);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o ocjeni nije moguće povezati s forme.");
                    return View(ocjenaPonude);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { id, page, sort, ascending });
            }
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pageSize = appSettings.PageSize;
            var query = ctx.OcjenaPonude.Include(i => i.sifraPonudeNavigation).Include(i => i.OIBOvlastenikaNavigation).AsNoTracking();

            int count = query.Count();
            //if count je nula preusmjerit na drugu str

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pageSize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<OcjenaPonude, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = ocj => ocj.sifraPonude;
                    break;
                case 2:
                    orderSelector = ocj => ocj.OIBOvlastenikaNavigation.ime + ocj.OIBOvlastenikaNavigation.prezime;
                    break;
                case 3:
                    orderSelector = ocj => ocj.ocjena;
                    break;
                case 4:
                    orderSelector = ocj => ocj.datum;
                    break;
                case 5:
                    orderSelector = ocj => ocj.opis;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var ocjenePonuda = query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var model = new OcjenePonudaViewModel
            {
                OcjenePonuda = ocjenePonuda,
                PagingInfo = pagingInfo
            };
            return View(model);
        }
    }
}
