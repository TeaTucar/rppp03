﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;

namespace WebApplicationJaNa.Controllers
{


    public class DPPonudaController : Controller
    {

        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;


        public DPPonudaController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appSettings = options.Value;

        }


        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Ponuda.AsNoTracking();
            int count = await query.CountAsync();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };


            //query = query.ApplySort(sort, ascending);

            var ponude = await query
                                .Select(l => new DPPonudaViewModel
                                {
                                    sifraPonude = l.sifraPonude,
                                    OIBPonuditelj = l.OIBPonuditelj,
                                    sifraKonzorcij = l.sifraKonzorcij,
                                    sifraNatjecaj = l.sifraNatjecaj
                                })
                                .Skip((page - 1) * pagesize)
                                .Take(pagesize)
                                .ToListAsync();

            var model = new DPPonudeViewModel
            {
                Ponude = ponude,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


        [HttpGet]
        public IActionResult Create() { return View(); }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Ponuda ponuda)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(ponuda);
                    await ctx.SaveChangesAsync();

                    TempData[Constants.Message] = $"Ponuda dodana. Sifra ponude = {ponuda.sifraPonude}";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(ponuda);
                }
            }
            else
            {

                return View(ponuda);
            }
        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int sifraPonude, int page = 1, int sort = 1, bool ascending = true)
        {
            var ponuda = await ctx.Ponuda.FindAsync(sifraPonude);
            if (ponuda != null)
            {
                try
                {

                    ctx.Remove(ponuda);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Ponuda sa šifrom {sifraPonude} je obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ponude: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji ponuda sa šifrom: {sifraPonude}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }



        [HttpGet]
        public async Task<IActionResult> Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {

            var ponuda = await ctx.Ponuda
                                  .AsNoTracking()
                                  .Where(l => l.sifraPonude == id)
                                  .SingleOrDefaultAsync();
            if (ponuda != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(ponuda);
            }
            else
            {
                return NotFound($"Neispravna sifra ponude: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Ponuda ponuda, int page = 1, int sort = 1, bool ascending = true)
        {
            if (ponuda == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = await ctx.Ponuda.AnyAsync(l => l.sifraPonude == ponuda.sifraPonude);
            if (!checkId)
            {
                return NotFound($"Neispravna šifra ponude: {ponuda?.sifraPonude}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(ponuda);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = "Ponuda ažurirana.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index), new { page, sort, ascending });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(ponuda);
                }
            }
            else
            {

                return View(ponuda);
            }
        }

    }


}
