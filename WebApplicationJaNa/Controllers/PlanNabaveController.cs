﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using Microsoft.AspNetCore.Mvc.Rendering;
using PdfRpt.FluentInterface;
using PdfRpt.Core.Contracts;

namespace WebApplicationJaNa.Controllers
{
    public class PlanNabaveController : Controller
    {
        private readonly RPPP03Context context;
        private readonly AppSettings appSettings;
        private readonly ILogger<PlanNabaveController> logger;


        public PlanNabaveController(RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<PlanNabaveController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }



        private async Task PrepareDropdownLists()
        {
            var narucitelj = await context.Narucitelj.OrderBy(t => t.naziv).Select(t => new { t.naziv, t.OIBNarucitelja }).ToListAsync();
            ViewBag.Narucitelji = new SelectList(narucitelj, nameof(Narucitelj.OIBNarucitelja), nameof(Narucitelj.naziv));
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = context.PlanNabave.Include(t => t.OIBNaruciteljaNavigation).AsNoTracking();
            int count = query.Count();

            if (count == 0)
            {
                logger.LogInformation("Ne postoje planovi nabave.");
                TempData[Constants.Message] = "Ne postoje planovi nabave.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages || page < 1)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var planovi = query. 
                                    Skip((page - 1) * pagesize)
                                 .Take(pagesize)
                                 .ToList();

            var model = new PlanNabaveViewModel
            {
                PlanNabave = planovi,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var planNabave = context.PlanNabave
                .AsNoTracking()
                .Where(t => t.idPlana == id)
                .SingleOrDefault();

            if (planNabave == null)
            {
                return NotFound("Ne postoji područje zanimanja sa šifrom:" + id);
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(planNabave);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                PlanNabave planNabave = await context.PlanNabave.FindAsync(id);

                if (planNabave == null)
                {
                    return NotFound("Neispravna šifra plana: " + id);
                }


                if (await TryUpdateModelAsync<PlanNabave>(planNabave, "", t => t.godina, t => t.OIBNarucitelja))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;

                    try
                    {
                        await context.SaveChangesAsync();
                        TempData[Constants.Message] = "Podaci o planu nabave su ažurirani.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        await PrepareDropdownLists();
                        return View(planNabave);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o planu nabave nije moguće povezati s forme");
                    return View(planNabave);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { page = page, sort = sort, ascending = ascending });
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string idPlana, int page = 1, int sort = 1, bool ascending = true)
        {
            var planNabave = context.PlanNabave.Find(idPlana);
            if (planNabave != null)
            {
                try
                {
                    int naziv = planNabave.godina;
                    context.Remove(planNabave);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Plan nabave za {naziv} uspješno obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception ex)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja plana nabave: " + ex.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Plan nabave s oznakom: " + idPlana;
                TempData[Constants.ErrorOccurred] = true;
                return NotFound();

            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });

        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownLists();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PlanNabave planNabave)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Add(planNabave);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Plan nabave {planNabave.idPlana} dodan.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(planNabave);
                }
            }
            else
            {
                return View(planNabave);
            }
        }


        /// <summary>
        /// Export u pdf datoteku
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> PDFReport()
        {
            string naslov = "Planovi nabava";
            var planovi = await context.PlanNabave
                .Include(o=> o.OIBNaruciteljaNavigation)
                .AsNoTracking()
                .ToListAsync();
            PdfReport report = Constants.CreateBasicReport(naslov);
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(planovi));

            report.MainTableColumns(columns =>
            {
                columns.AddColumn(column =>
                {
                    column.PropertyName<PlanNabave>(o => o.idPlana);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(0);
                    column.Width(4);
                    column.HeaderCell("Šifra plana", horizontalAlignment: HorizontalAlignment.Center);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<PlanNabave>(o => o.godina);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Left);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(4);
                    column.HeaderCell("Godina", horizontalAlignment: HorizontalAlignment.Left);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<PlanNabave>(o => o.OIBNaruciteljaNavigation.naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(2);
                    column.Width(2);
                    column.HeaderCell("Naziv naručitelja", horizontalAlignment: HorizontalAlignment.Center);
                });

            });


            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Planovi.pdf");
                return File(pdf, "application/pdf");
            }
            else
            {
                return NotFound();
            }

        }

    }
}
