﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using WebApplicationJaNa.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class AVNaruciteljController : Controller
    {

        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public AVNaruciteljController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }

        private async Task PrepareDropdownList()
        {
            var gradovi = await ctx.Lokacija.OrderBy(t => t.grad).Select(t => new { t.grad, t.sifraGrad }).ToListAsync();
            ViewBag.Narucitelj = new SelectList(gradovi, nameof(Lokacija.sifraGrad), nameof(Lokacija.grad));
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownList();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Narucitelj narucitelj)
        {
            try
            {
                ctx.Add(narucitelj);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Naručitelj {narucitelj.naziv} uspješno dodan.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                return View(narucitelj);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string OIBNarucitelja, int page = 1, int sort = 1, bool ascending = true)
        {
            var narucitelj = ctx.Narucitelj
                .Find(OIBNarucitelja);
            if (narucitelj == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    string oib = narucitelj.OIBNarucitelja;
                    ctx.Remove(narucitelj);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Naručitelj {narucitelj.naziv} uspješno obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja naručitelja.";
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }
        [HttpGet]
        public async Task<IActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            await PrepareDropdownList();
            var narucitelj = ctx.Narucitelj.
                AsNoTracking()
                .Where(n => n.OIBNarucitelja == id)
                .FirstOrDefault();
            if (narucitelj == null)
            {
                return NotFound($"Ne postoji naručitelj s OIB-om {id}");
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(narucitelj);
            }
        }
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Narucitelj narucitelj = await ctx.Narucitelj.FindAsync(id);
                if (narucitelj == null)
                {
                    return NotFound($"Ne postoji naručitelj s OIB-om {id}");
                }

                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;

                bool ok = await TryUpdateModelAsync<Narucitelj>(narucitelj, "", n => n.naziv, n => n.sifraGrad, n => n.adresa);
                if (ok)
                {
                    try
                    {
                        TempData[Constants.Message] = $"Naručitelj {narucitelj.naziv} uspješno ažuriran.";
                        TempData[Constants.ErrorOccurred] = false;
                        await ctx.SaveChangesAsync();
                        return RedirectToAction(nameof(Index), new { page, sort, ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(narucitelj);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o naručitelju nije moguće povezati s forme.");
                    return View(narucitelj);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { id, page, sort, ascending });
            }
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pageSize = appSettings.PageSize;
            var query = ctx.Narucitelj.Include(i => i.sifraGradNavigation).AsNoTracking();

            int count = query.Count();
            //if count je nula preusmjerit na drugu str

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pageSize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Narucitelj, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = nar => nar.OIBNarucitelja;
                    break;
                case 2:
                    orderSelector = nar => nar.naziv;
                    break;
                case 3:
                    orderSelector = nar => nar.adresa;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var narucitelji = query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var model = new AVNaruciteljiViewModel
            {
                Narucitelji = narucitelji,
                PagingInfo = pagingInfo
            };
            return View(model);
        }
    }
}
