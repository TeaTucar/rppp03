﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;

namespace WebApplicationJaNa.Controllers
{
    public class TTPonudaController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public TTPonudaController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Ponuda
                           .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                TempData[Constants.Message] = "Ne postoji niti jedna ponuda.";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(HomeController.View));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };
            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = 1, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);


            var ponude = await query
                          .Select(p => new TTPonudaViewModel
                          {
                              sifraPonude = p.sifraPonude,
                              nazivPonuditelj = p.OIBPonuditeljNavigation.nazivPonuditelj,
                              nazivKonzorcij = p.sifraKonzorcijNavigation.nazivKonzoricij,
                              sifraNatjecaj = p.sifraNatjecaj,
                          })
                          .Skip((page - 1) * pagesize)
                          .Take(pagesize)
                          .ToListAsync();

            foreach (TTPonudaViewModel ponuda in ponude)
            {
                ponuda.stavkeTroskovnika = ctx.IspunjenaStavka
                                              .AsNoTracking()
                                              .dohvatiStavke(ponuda.sifraPonude);
            }

            var model = new TTPonudeViewModel
            {
                Ponude = ponude,
                PagingInfo = pagingInfo
            };

            return View(model);
        }

        //detaljniji prikaz detail-a
        [HttpGet]
        public async Task<IActionResult> More(int id, int page = 1, int sort = 1, bool ascending = true, string viewName = nameof(More))
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.IspunjenaStavka
                           .AsNoTracking();

            int count = query.Count(s => s.sifraPonude == id);
            /*if (count == 0)
            {
                TempData[Constants.Message] = "Ne postoji niti jedna stavka.";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(HomeController.View));
            }*/

            var pagingInfo = new PagingInfo //maknuti paging info iz StavkeViewModel
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = 5  //count
            };
            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(More), new { id, page = 1, sort, ascending });
            }

            List<StavkaTroskovnika> stavke = new List<StavkaTroskovnika>();

            //if (count > 0)
            {
                //lista id stavaka koje odgovaraju ponudi
                var brStavke = await query.Where(s => s.sifraPonude == id)
                                          .Select(s => s.brStavke)
                                          .ToListAsync();

                //lista stavaka koje odgovaraju ponudi
                foreach (int br in brStavke)
                {
                    stavke.Add(ctx.StavkaTroskovnika.AsQueryable().First(s => s.idStavke == br));
                }
            }

            var model = await ctx.Ponuda
                                 .AsQueryable()
                                 .Where(p => p.sifraPonude == id)
                                 .Select(p => new StavkeViewModel
                                 {
                                    ponuda = p,
                                    PagingInfo = pagingInfo,
                                    stavke = stavke,
                                    nazivPonuditelja = p.OIBPonuditeljNavigation.nazivPonuditelj,
                                    nazivKonzorcija = p.sifraKonzorcijNavigation.nazivKonzoricij,
                                 })
                                 .ToListAsync();

            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            return View(model.ElementAt(0));
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id, int page = 1, int sort = 1, bool ascending = true, string viewName = nameof(More))
        {
            return await More(id, page, sort, ascending, nameof(Edit));     //da koristi istu metodu kao za More, ali da vrati Edit.cshtml
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(StavkeViewModel model, int id, int page = 1, int sort = 1, bool ascending = true)
        {
            ViewBag.Page = page;
            ViewBag.Sort = sort;
            ViewBag.Ascending = ascending;

            if (ModelState.IsValid)
            {
                var ponuda = ctx.Ponuda
                                .Where(p => p.sifraPonude == model.ponuda.sifraPonude)
                                .FirstOrDefault();

                //lista id-eva svih stavki odgovarajuce ponude
                var stavkeId = ctx.IspunjenaStavka
                                  .Where(s => s.sifraPonude == model.ponuda.sifraPonude)
                                  .Select(s => s.brStavke)
                                  .ToList();

                if (ponuda == null)
                {
                    return NotFound("Ne postoji ponuda sa šifrom: " + model.ponuda.sifraPonude);
                }

                ponuda.OIBPonuditelj = model.ponuda.OIBPonuditelj;
                ponuda.sifraKonzorcij = model.ponuda.sifraKonzorcij;

                try
                {
                    //svi id stavki koje nisu uklonjene
                    List<int> idStavki = model.stavke
                                              .Where(s => s.idStavke > 0)
                                              .Select(s => s.idStavke)
                                              .ToList();
                    //dohvacanje tih stavki
                    List<StavkaTroskovnika> popisStavki = new List<StavkaTroskovnika>();
                    foreach (int br in idStavki)
                    {
                        popisStavki.Add(ctx.StavkaTroskovnika.AsQueryable().First(s => s.idStavke == br));
                    }

                    //uklanjanje svih koje su obrisane
                    ctx.RemoveRange(popisStavki.Where(s => !idStavki.Contains(s.idStavke)));

                    foreach(var stavka in model.stavke)
                    {
                        StavkaTroskovnika novaStavka;
                        if (stavka.idStavke > 0) {
                            novaStavka = popisStavki.First(s => s.idStavke == stavka.idStavke);
                        }
                        else {
                            novaStavka = new StavkaTroskovnika();
                            novaStavka.idStavke = stavka.idStavke;
                            popisStavki.Add(novaStavka);
                        }
                        novaStavka.kolicina = stavka.kolicina;
                        novaStavka.cijena = stavka.cijena;
                    }

                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Ponuda {ponuda.sifraPonude} uspješno ažurirana";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Edit),
                        new
                        {
                            id = ponuda.sifraPonude,
                            page,
                            sort,
                            ascending
                        });
                }
                catch(Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(model);
                }

            }
            else
            {
                return View(model);
            }
        }
    }
}
