﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using PdfRpt.FluentInterface;
using PdfRpt.Core.Contracts;

namespace WebApplicationJaNa.Controllers
{
    public class TTPonuditeljController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public TTPonuditeljController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Ponuditelj
                           .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                TempData[Constants.Message] = "Ne postoji niti jedna država.";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };
            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = 1, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var ponuditelji = await query
                          .Select(p => new TTPonuditeljViewModel
                          {
                              nazivPonuditelj = p.nazivPonuditelj,
                              OIBPonuditelj = p.OIBPonuditelj,
                              adresa = p.adresa,
                              grad = p.sifraGradNavigation.grad,
                              sifraGrad = p.sifraGrad,
                              drzava = p.sifraGradNavigation.drzava
                          })
                          .Skip((page - 1) * pagesize)
                          .Take(pagesize)
                          .ToListAsync();

            var model = new TTPonuditeljiViewModel
            {
                Ponuditelji = ponuditelji,
                PagingInfo = pagingInfo
            };

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await prepareDropdownList();
            return View();
        }

        private async Task prepareDropdownList()
        {
            var mjesto = await ctx.Lokacija
                        .Select(m => new { m.sifraGrad, m.grad })
                        .OrderBy(m => m.grad)
                        .ToListAsync();

            ViewBag.Mjesta = new SelectList(mjesto, nameof(Lokacija.sifraGrad), nameof(Lokacija.grad));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Ponuditelj ponuditelj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(ponuditelj);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Ponuditelj {ponuditelj.nazivPonuditelj} je dodan.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(String.Empty, exc.CompleteExceptionMessage());
                    return View(ponuditelj);
                }
            } else
            {
                return View(ponuditelj);
            }

        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var ponuditelj = ctx.Ponuditelj
                                .AsNoTracking()
                                .Where(p => p.OIBPonuditelj == id)
                                .FirstOrDefault();
            if (ponuditelj == null)
            {
                return NotFound("Ne postoji ponuditelj s OIB-om: " + id);
            }
            else
            {
                await prepareDropdownList();
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(ponuditelj);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Ponuditelj ponuditelj = await ctx.Ponuditelj
                                  .Where(p => p.OIBPonuditelj == id)
                                  .FirstOrDefaultAsync();
                if (ponuditelj == null)
                {
                    return NotFound("Neispravan OIB ponuditelja: " + id);
                }

                if (await TryUpdateModelAsync<Ponuditelj>(ponuditelj, "",
                    p => p.nazivPonuditelj, p => p.OIBPonuditelj, p => p.sifraGrad
                ))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;
                    try
                    {
                        await ctx.SaveChangesAsync();
                        TempData[Constants.Message] = "Ponuditelj ažuriran.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        await prepareDropdownList();
                        return View(ponuditelj);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Nije moguće povezati podatke s ponuditeljem.");
                    await prepareDropdownList();
                    return View(ponuditelj);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), id);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(String OIBPonuditelj, int page=1, int sort=1, bool ascending=true)
        {
            var ponuditelj = ctx.Ponuditelj.Find(OIBPonuditelj);
            if (ponuditelj != null)
            {
                try
                {
                    string naziv = ponuditelj.nazivPonuditelj;
                    ctx.Remove(ponuditelj);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuditelj {naziv} uspješno obrisan";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ponuditelja: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Ne postoji ponuditelj s OIB-om: " + OIBPonuditelj;
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
        }

        /// <summary>
        /// Export u pdf datoteku
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> PDFReport()
        {
            string naslov = "Ponuditelji";
            var planovi = await ctx.Ponuditelj
                .Include(o => o.sifraGradNavigation)
                .AsNoTracking()
                .ToListAsync();
            PdfReport report = Constants.CreateBasicReport(naslov);
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(planovi));

            report.MainTableColumns(columns =>
            {
                columns.AddColumn(column =>
                {
                    column.PropertyName<Ponuditelj>(o => o.nazivPonuditelj);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(0);
                    column.Width(4);
                    column.HeaderCell("Naziv", horizontalAlignment: HorizontalAlignment.Center);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<Ponuditelj>(o => o.OIBPonuditelj);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Left);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(4);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Left);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<Ponuditelj>(o => o.adresa);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(4);
                    column.HeaderCell("Adresa", horizontalAlignment: HorizontalAlignment.Center);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<Ponuditelj>(o => o.sifraGradNavigation.grad);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Left);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(4);
                    column.HeaderCell("Grad", horizontalAlignment: HorizontalAlignment.Left);
                });
                columns.AddColumn(column =>
                {
                    column.PropertyName<Ponuditelj>(o => o.sifraGradNavigation.drzava);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Left);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(4);
                    column.HeaderCell("Država", horizontalAlignment: HorizontalAlignment.Left);
                });

            });


            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Ponuditelji.pdf");
                return File(pdf, "application/pdf");
            }
            else
            {
                return NotFound();
            }

        }
    }
}
