﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using WebApplicationJaNa.Extensions;

namespace WebApplicationJaNa.Controllers
{
    public class AVPonudaController : Controller
    {

        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public AVPonudaController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            appSettings = optionsSnapshot.Value;
        }
        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pageSize = appSettings.PageSize;
            var query = ctx.Ponuda.Include(i => i.sifraKonzorcijNavigation).Include(i => i.OIBPonuditeljNavigation)
                                    .Include(i => i.sifraNatjecajNavigation.idNabavaNavigation).AsNoTracking();

            int count = query.Count();
            //if count je nula preusmjerit na drugu str

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pageSize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            System.Linq.Expressions.Expression<Func<Ponuda, object>> orderSelector = null;
            switch (sort)
            {
                case 1:
                    orderSelector = p => p.sifraPonude;
                    break;
                case 2:
                    orderSelector = p => p.OIBPonuditeljNavigation.nazivPonuditelj;
                    break;
                case 3:
                    orderSelector = p => p.sifraKonzorcijNavigation.nazivKonzoricij;
                    break;
                case 4:
                    orderSelector = p => p.sifraNatjecajNavigation.idNabavaNavigation.predmet;
                    break;
            }

            if (orderSelector != null)
            {
                query = ascending ? query.OrderBy(orderSelector) : query.OrderByDescending(orderSelector);
            }

            var ponude = query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var model = new AVPonudeViewModel
            {
                Ponude = ponude,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int sifraPonude, int page = 1, int sort = 1, bool ascending = true)
        {
            var ponuda = ctx.Ponuda
                .Find(sifraPonude);
            if (ponuda == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    var sifra = ponuda.sifraPonude;
                    ctx.Remove(ponuda);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Ponuda šifre {ponuda.sifraPonude} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ponude.";
                    TempData[Constants.ErrorOccurred] = true;
                }
                return RedirectToAction(nameof(Index), new { page, sort, ascending });
            }
        }

        private async Task PrepareDropdownList()
        {
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownList();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Ponuda ponuda)
        {
            try
            {
                ctx.Add(ponuda);
                ctx.SaveChanges();
                TempData[Constants.Message] = $"Ponuda šifre {ponuda.sifraPonude} je uspješno dodana.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                return View(ponuda);
            }
        }
    }
}
