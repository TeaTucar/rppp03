﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Extensions.Selectors;

namespace WebApplicationJaNa.Controllers
{
    public class DokumentController : Controller
    {
        private readonly Models.RPPP03Context context;
        private readonly ILogger<NatjecajController> logger;
        private readonly AppSettings appSettings;
        public DokumentController(Models.RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<NatjecajController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }

        public IActionResult Index(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;

            var query = context.Dokument
                        .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                logger.LogInformation("Niti jedan dokument nije evidentiran za natječaj ...");
                TempData[Constants.Message] = "Niti jedan dokument nije evidentiran za natječaj ...";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Index));
            }

            var pagingInfo = new ViewModels.PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new
                {
                    page = pagingInfo.TotalPages,
                    sort,
                    ascending
                });
            }
            query = query.ApplySort(sort, ascending);

            
            var natjecaj = context.Natjecaj.Find(id);
            if (natjecaj == null) natjecaj = context.Natjecaj.First();
            var dokumenti = query.Where(d => d.sifraNatjecaj == natjecaj.sifraNatjecaj)
                        .Skip((page - 1) * pagesize)
                        .Take(pagesize)
                        .ToList();
            var nabava = context.Nabava.Find(natjecaj.idNabava);
            var narucitelj = context.Narucitelj.Find(natjecaj.OIBNarucitelja);

            var model = new ViewModels.DokumentViewModel
            {
                Dokumenti = dokumenti,
                Natjecaj = natjecaj,
                Nabava = nabava,
                Narucitelj = narucitelj,
                PagingInfo = pagingInfo
            };
            return View(model);
        }
    }
}
