﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using WebApplicationJaNa.Extensions.Selectors;

namespace WebApplicationJaNa.Controllers
{
    public class NatjecajController : Controller
    {
        private readonly Models.RPPP03Context context;
        private readonly ILogger<NatjecajController> logger;
        private readonly AppSettings appSettings;

        public NatjecajController(Models.RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<NatjecajController> logger)
        {
            this.context = context;

            this.logger = logger;
            appSettings = options.Value;
        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;

            var query = context.Natjecaj
                        .Include(nat => nat.idNabavaNavigation)
                        .Include(nat => nat.OIBNaruciteljaNavigation)
                        .Include(nat => nat.Dokument)
                        .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                logger.LogInformation("Niti jedan natječaj nije evidentiran");
                TempData[Constants.Message] = "Niti jedan natječaj nije evidentiran";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Index));
            }

            var pagingInfo = new ViewModels.PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new
                {
                    page = pagingInfo.TotalPages,
                    sort,
                    ascending
                });
            }
            query = query.ApplySort(sort, ascending);

            var natjecaji = query
                        .Skip((page - 1) * pagesize)
                        .Take(pagesize)
                        .ToList();

            var model = new ViewModels.NatjecajViewModel
            {
                Natjecaji = natjecaji,
                PagingInfo = pagingInfo
            };
            return View(model);
        }
    }
}
