﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;

namespace WebApplicationJaNa.Controllers
{
    public class MMNatjecajController : Controller
    {
        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;

        public MMNatjecajController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> optionsSnapshot)
        {
            this.ctx = ctx;
            this.appSettings = optionsSnapshot.Value;
        }

        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Natjecaj
                           .AsNoTracking();

            int count = query.Count();
            if (count == 0)
            {
                TempData[Constants.Message] = "Ne postoji niti jedan natječaj.";
                TempData[Constants.ErrorOccurred] = false;
                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };
            if (page < 1 || page > pagingInfo.TotalPages)
            {
                return RedirectToAction(nameof(Index), new { page = 1, sort, ascending });
            }

            var natjecaji = await query
                          .Select(n => new MMNatjecajViewModel
                          {
                              sifraNatjecaj = n.sifraNatjecaj,
                              dostavaElektronickimPutem = n.dostavaElektronickimPutem,
                              idNabava = n.idNabava,
                              predmetNabave = n.idNabavaNavigation.predmet,
                              OIBNarucitelja = n.OIBNarucitelja,
                              nazivNarucitelja = n.OIBNaruciteljaNavigation.naziv,
                              datumObjave = n.datumObjave,
                              rokZaDostavu = n.rokZaDostavu
                          })
                          .Skip((page - 1) * pagesize)
                          .Take(pagesize)
                          .ToListAsync();

            var model = new MMNatjecajiViewModel
            {
                Natjecaji = natjecaji,
                PagingInfo = pagingInfo
            };

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await prepareDropdownListNabava();
            await prepareDropdownListNarucitelj();
            return View();
        }

        private async Task prepareDropdownListNarucitelj()
        {
            var narucitelji = await ctx.Narucitelj
                        .Select(n => new { n.OIBNarucitelja, n.naziv })
                        .OrderBy(n => n.naziv)
                        .ToListAsync();
            ViewBag.Narucitelji = new SelectList(narucitelji, nameof(Narucitelj.OIBNarucitelja), nameof(Narucitelj.naziv));
        }

        private async Task prepareDropdownListNabava()
        {
            var predmet = await ctx.Nabava
                        .Select(n => new { n.idNabava, n.predmet })
                        .OrderBy(n => n.predmet)
                        .ToListAsync();

            ViewBag.Predmeti = new SelectList(predmet, nameof(Nabava.idNabava), nameof(Nabava.predmet));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Natjecaj natjecaj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(natjecaj);
                    ctx.SaveChanges();

                    TempData[Constants.Message] = $"Ponuditelj {natjecaj.sifraNatjecaj} je dodan.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(String.Empty, exc.CompleteExceptionMessage());
                    return View(natjecaj);
                }
            }
            else
            {
                return View(natjecaj);
            }

        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            var natjecaj = ctx.Natjecaj
                                .AsNoTracking()
                                .Where(n => n.sifraNatjecaj == id)
                                .FirstOrDefault();
            if (natjecaj == null)
            {
                return NotFound("Ne postoji natjecaj sa šifrom: " + id);
            }
            else
            {
                await prepareDropdownListNabava();
                await prepareDropdownListNarucitelj();
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(natjecaj);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Natjecaj natjecaj = await ctx.Natjecaj
                                  .Where(n => n.sifraNatjecaj == id)
                                  .FirstOrDefaultAsync();
                if (natjecaj == null)
                {
                    return NotFound("Neispravna šifra natječaja: " + id);
                }

                if (await TryUpdateModelAsync<Natjecaj>(natjecaj, "",
                    n => n.dostavaElektronickimPutem, n => n.idNabava, n => n.OIBNarucitelja, n => n.datumObjave, n => n.rokZaDostavu
                ))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;
                    try
                    {
                        await ctx.SaveChangesAsync();
                        TempData[Constants.Message] = "Natječaj žuriran.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        await prepareDropdownListNabava();
                        await prepareDropdownListNarucitelj();
                        return View(natjecaj);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Nije moguće povezati podatke s natječajem.");
                    await prepareDropdownListNabava();
                    await prepareDropdownListNarucitelj();
                    return View(natjecaj);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), id);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int sifraNatjecaj, int page = 1, int sort = 1, bool ascending = true)
        {
            var natjecaj = ctx.Natjecaj.Find(sifraNatjecaj);
            if (natjecaj != null)
            {
                try
                {
                    string naziv = natjecaj.sifraNatjecaj.ToString();
                    ctx.Remove(natjecaj);
                    ctx.SaveChanges();
                    TempData[Constants.Message] = $"Natječaj {naziv} uspješno obrisan";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja natječaja: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Ne postoji natječaj sa šifrom: " + sifraNatjecaj;
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
        }
    }
}
