﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;

namespace WebApplicationJaNa.Controllers
{
    public class PodrucjeZanimanjaController : Controller
    {
        private readonly RPPP03Context context;
        private readonly AppSettings appSettings;
        private readonly ILogger<PodrucjeZanimanjaController> logger;


        public PodrucjeZanimanjaController(RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<PodrucjeZanimanjaController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }


        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = context.PodrucjeZanimanja.AsNoTracking();
            int count = query.Count();

            if (count == 0)
            {
                logger.LogInformation("Ne postoje područja zanimanja.");
                TempData[Constants.Message] = "Ne postoje područja zanimanja.";
                TempData[Constants.ErrorOccurred] = false;
               
                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages || page < 1)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var podrucjaZanimanja = query.Skip((page - 1) * pagesize)
                                 .Take(pagesize)
                                 .ToList();

            var model = new PodrucjeZanimanjaViewModel
            {
                PodrucjeZanimanja = podrucjaZanimanja,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var podrucjeZanimanja = context.PodrucjeZanimanja
                .AsNoTracking()
                .Where(t => t.idPodrucja == id)
                .SingleOrDefault();

            if (podrucjeZanimanja == null)
            {
                return NotFound("Ne postoji područje zanimanja sa šifrom:" + id);
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(podrucjeZanimanja);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                PodrucjeZanimanja podrucjeZanimanja = await context.PodrucjeZanimanja.FindAsync(id);

                if (podrucjeZanimanja == null)
                {
                    return NotFound("Neispravna šifra područja zanimanja: " +id);
                }


                if (await TryUpdateModelAsync<PodrucjeZanimanja>(podrucjeZanimanja, "", t => t.nazivPodrucja))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;

                    try
                    {
                        await context.SaveChangesAsync();
                        TempData[Constants.Message] = "Podaci o području zanimanja su ažurirani.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        return View(podrucjeZanimanja);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o području zanimanja nije moguće povezati s forme");
                    return View(podrucjeZanimanja);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { page = page, sort = sort, ascending = ascending });
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string idPodrucja, int page = 1, int sort = 1, bool ascending = true)
        {
            var podrucjeZanimanja = context.PodrucjeZanimanja.Find(idPodrucja);
            if (podrucjeZanimanja != null)
            {
                try
                {
                    string naziv = podrucjeZanimanja.nazivPodrucja;
                    context.Remove(podrucjeZanimanja);
                    context.SaveChanges();
                    TempData[Constants.Message] =$"Područje zanimanja {naziv} uspješno obrisano.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception ex)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja područja zanimanja: " + ex.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Područje zanimanja s oznakom: " + idPodrucja;
                TempData[Constants.ErrorOccurred] = true;
                return NotFound();
                
            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });

        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PodrucjeZanimanja podrucjeZanimanja)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Add(podrucjeZanimanja);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Područje zanimanja {podrucjeZanimanja.idPodrucja} dodano.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.CompleteExceptionMessage());
                    return View(podrucjeZanimanja);
                }
            }
            else
            {
                return View(podrucjeZanimanja);
            }
        }


    }
}
