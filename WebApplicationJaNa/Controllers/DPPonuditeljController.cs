﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using WebApplicationJaNa.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{


    public class DPPonuditeljController : Controller
    {

        private readonly RPPP03Context ctx;
        private readonly AppSettings appSettings;


        public DPPonuditeljController(RPPP03Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appSettings = options.Value;

        }


        public async Task<IActionResult> Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = ctx.Ponuditelj.AsNoTracking();
            int count = await query.CountAsync();

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };


            query = query.ApplySort(sort, ascending);

            var ponuditelji = await query
                                .Select(l => new DPPonuditeljViewModel
                                {
                                    OIBPonuditelj = l.OIBPonuditelj,
                                    nazivPonuditelj = l.nazivPonuditelj,
                                    grad = l.sifraGradNavigation.grad,
                                    sifraGrad = l.sifraGrad,
                                    drzava = l.sifraGradNavigation.drzava,
                                    adresa = l.adresa
                                })
                                .Skip((page - 1) * pagesize)
                                .Take(pagesize)
                                .ToListAsync();

            var model = new DPPonuditeljiViewModel
            {
                Ponuditelji = ponuditelji,
                PagingInfo = pagingInfo
            };

            return View(model);
        }


        private async Task prepareDropdownList()
        {
            var lokacija = await ctx.Lokacija
                        .Select(l => new { l.sifraGrad, l.grad })
                        .ToListAsync();
            ViewBag.Mjesta = new SelectList(lokacija, nameof(Lokacija.sifraGrad), nameof(Lokacija.grad));
        }

        [HttpGet]
        public async Task<IActionResult> CreateAsync()
        {
            await prepareDropdownList();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Ponuditelj ponuditelj)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Add(ponuditelj);
                    await ctx.SaveChangesAsync();

                    TempData[Constants.Message] = $"Ponuditelj OIB-a  {ponuditelj.OIBPonuditelj} je dodan";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(ponuditelj);
                }
            }
            else
            {

                return View(ponuditelj);
            }
        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string OIBPonuditelj, int page = 1, int sort = 1, bool ascending = true)
        {
            var ponuditelj = await ctx.Ponuditelj.FindAsync(OIBPonuditelj);
            if (ponuditelj != null)
            {
                try
                {
                    string naziv = ponuditelj.nazivPonuditelj;
                    ctx.Remove(ponuditelj);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = $"Ponuđač naziva {naziv} sa šifrom {OIBPonuditelj} obrisan.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception exc)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja ponuđača: " + exc.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = $"Ne postoji ponuđač sa OIB-om: {OIBPonuditelj}";
                TempData[Constants.ErrorOccurred] = true;
            }
            return RedirectToAction(nameof(Index), new { page, sort, ascending });
        }



        [HttpGet]
        public async Task<IActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            await prepareDropdownList();

            var ponuditelj = await ctx.Ponuditelj
                                  .AsNoTracking()
                                  .Where(l => l.OIBPonuditelj == id)
                                  .SingleOrDefaultAsync();
            if (ponuditelj != null)
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                return View(ponuditelj);
            }
            else
            {
                return NotFound($"Neispravni OIB ponuditelja: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Ponuditelj ponuditelj, int page = 1, int sort = 1, bool ascending = true)
        {
            if (ponuditelj == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = await ctx.Ponuditelj.AnyAsync(l => l.OIBPonuditelj == ponuditelj.OIBPonuditelj);
            if (!checkId)
            {
                return NotFound($"Neispravna šifra lokacije: {ponuditelj?.OIBPonuditelj}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    ctx.Update(ponuditelj);
                    await ctx.SaveChangesAsync();
                    TempData[Constants.Message] = "Ponuditelj ažuriran.";
                    TempData[Constants.ErrorOccurred] = false;
                    return RedirectToAction(nameof(Index), new { page, sort, ascending });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());

                    return View(ponuditelj);
                }
            }
            else
            {

                return View(ponuditelj);
            }
        }

    }
}
