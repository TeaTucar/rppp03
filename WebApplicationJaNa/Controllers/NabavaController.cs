﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationJaNa.Models;
using WebApplicationJaNa.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationJaNa.Extensions;
using WebApplicationJaNa.Extensions.Selectors;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplicationJaNa.Controllers
{
    public class NabavaController : Controller
    {
        private readonly RPPP03Context context;
        private readonly AppSettings appSettings;
        private readonly ILogger<NabavaController> logger;


        public NabavaController(RPPP03Context context, IOptionsSnapshot<AppSettings> options, ILogger<NabavaController> logger)
        {
            this.context = context;
            this.logger = logger;
            appSettings = options.Value;
        }



        private async Task PrepareDropdownLists()
        {
            var planovi = await context.PlanNabave.OrderBy(t => t.godina).Select(t => new { t.godina, t.idPlana }).ToListAsync();
            ViewBag.Planovi = new SelectList(planovi, nameof(PlanNabave.idPlana), nameof(PlanNabave.godina));

            var podrucja = await context.PodrucjeZanimanja.OrderBy(t => t.nazivPodrucja).Select(t => new { t.nazivPodrucja, t.idPodrucja }).ToListAsync();
            ViewBag.Podrucja = new SelectList(podrucja, nameof(PodrucjeZanimanja.idPodrucja), nameof(PodrucjeZanimanja.nazivPodrucja));

        }

        public IActionResult Index(int page = 1, int sort = 1, bool ascending = true)
        {
            int pagesize = appSettings.PageSize;
            var query = context.Nabava.Include(t =>  t.idPodrucjaNavigation).Include(t => t.idPlanaNavigation).AsNoTracking();
            int count = query.Count();

            if (count == 0)
            {
                logger.LogInformation("Ne postoje nabave.");
                TempData[Constants.Message] = "Ne postoje nabave.";
                TempData[Constants.ErrorOccurred] = false;

                return RedirectToAction(nameof(Create));
            }

            var pagingInfo = new PagingInfo
            {
                CurrentPage = page,
                Sort = sort,
                Ascending = ascending,
                ItemsPerPage = pagesize,
                TotalItems = count
            };

            if (page > pagingInfo.TotalPages || page < 1)
            {
                return RedirectToAction(nameof(Index), new { page = pagingInfo.TotalPages, sort, ascending });
            }

            query = query.ApplySort(sort, ascending);

            var nabave = query.
                                    Skip((page - 1) * pagesize)
                                 .Take(pagesize)
                                 .ToList();

            var model = new NabavaViewModel
            {
                Nabava = nabave,
                PagingInfo = pagingInfo
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            var nabave = context.Nabava
                .AsNoTracking()
                .Where(r => r.idNabava == id)
                .SingleOrDefault();

            if (nabave == null)
            {
                return NotFound("Ne postoji nabava sa šifrom:" + id);
            }
            else
            {
                ViewBag.Page = page;
                ViewBag.Sort = sort;
                ViewBag.Ascending = ascending;
                await PrepareDropdownLists();
                return View(nabave);
            }
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(string id, int page = 1, int sort = 1, bool ascending = true)
        {
            try
            {
                Nabava nabave = await context.Nabava.FindAsync(id);

                if (nabave == null)
                {
                    return NotFound("Neispravna šifra nabave: " + id);
                }


                if (await TryUpdateModelAsync<Nabava>(nabave, "", t => t.predmet, t => t.CPV, t => t.CPV, t => t.procVrijednost,
                                                t => t.vrstaPostupka, t => t.podjelaGrupe, t => t.planiraniPocetak, t => t.vrijediOd,
                                                t => t.vrijediDo, t => t.zakon, t => t.idPodrucja, t => t.idPlana))
                {
                    ViewBag.Page = page;
                    ViewBag.Sort = sort;
                    ViewBag.Ascending = ascending;

                    try
                    {
                        await context.SaveChangesAsync();
                        TempData[Constants.Message] = "Podaci o nabavi su ažurirani.";
                        TempData[Constants.ErrorOccurred] = false;
                        return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });
                    }
                    catch (Exception exc)
                    {
                        ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                        await PrepareDropdownLists();
                        return View(nabave);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Podatke o nabavi nije moguće povezati s forme");
                    return View(nabave);
                }
            }
            catch (Exception exc)
            {
                TempData[Constants.Message] = exc.CompleteExceptionMessage();
                TempData[Constants.ErrorOccurred] = true;
                return RedirectToAction(nameof(Edit), new { page = page, sort = sort, ascending = ascending });
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string idNabava, int page = 1, int sort = 1, bool ascending = true)
        {
            var nabave = context.Nabava.Find(idNabava);
            if (nabave != null)
            {
                try
                {
                    String naziv = nabave.predmet;
                    context.Remove(nabave);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Nabava za {naziv} uspješno obrisana.";
                    TempData[Constants.ErrorOccurred] = false;
                }
                catch (Exception ex)
                {
                    TempData[Constants.Message] = "Pogreška prilikom brisanja nabave: " + ex.CompleteExceptionMessage();
                    TempData[Constants.ErrorOccurred] = true;
                }
            }
            else
            {
                TempData[Constants.Message] = "Nabava s oznakom: " + idNabava;
                TempData[Constants.ErrorOccurred] = true;
                return NotFound();

            }
            return RedirectToAction(nameof(Index), new { page = page, sort = sort, ascending = ascending });

        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            await PrepareDropdownLists();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Nabava nabave)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Add(nabave);
                    context.SaveChanges();
                    TempData[Constants.Message] = $"Nabava {nabave.idNabava} dodana.";
                    TempData[Constants.ErrorOccurred] = false;

                    return RedirectToAction(nameof(Index));

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.CompleteExceptionMessage());
                    await PrepareDropdownLists();
                    return View(nabave);
                }
            }
            else
            {
                return View(nabave);
            }
        }


    }
}

